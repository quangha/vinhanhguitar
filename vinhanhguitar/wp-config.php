<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'scotchbox');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'P.ZG5!v_k2^z $AQKa03>| |~^[0,eVv$h/Xr*S5Tilz#>z^2YIf%TfRLEKbYum?');
define('SECURE_AUTH_KEY',  'epsU0HE79C&|gmCDd>djWc{jg?2(k#BCEJxl>RCLZxh~E}VQAF#^&YVj6qKH+i0$');
define('LOGGED_IN_KEY',    'Goq!f+a?Y2e:-,Wh;BnUB.DE5=umg7uQ1IZ-xC#N:Dc7;a4VVC:JBAop~Zr}xO?^');
define('NONCE_KEY',        '),JZjM4I-c=*zkg;eR7jmq-lW f1Xkz?G*TD>LFxR@P$6|UEo8tnW2PRN V*oM1&');
define('AUTH_SALT',        'J-m%}`pft+s%>O^dJ|AN+H<8tFp)RvWO}{<~n+857-[9_1xHEG!e|iQABJi^4^ko');
define('SECURE_AUTH_SALT', 'P-Pk@Ryr,@W#A.<GD>i{|kCmn^U0FQ=fn&:z>~fB(Ztg;@S|UFuqLHowkvkC{8jG');
define('LOGGED_IN_SALT',   'dc%yHh97$0%K&)8QXWZ[_v0!p+uKb )?G`gQ63rmy<U}sa<dURq3#B}=,m8F-kz8');
define('NONCE_SALT',       'Uh75_  lXm*8e>ffwf47h[S=bG|%O!:3}mt)3wj4vj;1D*5&e|BbX6)Gdo!b5$c>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
